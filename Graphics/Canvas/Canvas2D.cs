﻿using Bitz.Modules.Core.Graphics.Canvases;
using Bitz.Modules.Platform.Android.Graphics.Cameras;
using Bitz.Modules.Platform.Android.Graphics.Shaders.Stock;
using OpenTK.Graphics.ES30;


namespace Bitz.Modules.Platform.Android.Graphics.Canvas
{
    /// <summary>
    /// A derivation of the Canvas class that is configured for 2D usage
    /// </summary>
    public class Canvas2D : Core.Graphics.Canvases.Canvas , ICanvas2D
    {
        public Canvas2D() : base (new Camera2D(), new Stock2D())
        {
            Visible = true;
        }

        public override void ConfigureGlState()
        {
            GL.Enable(EnableCap.Blend);
            GL.Disable(EnableCap.DepthTest);
            GL.Disable(EnableCap.StencilTest);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }

        /// <inheritdoc />
        public void Initialize()
        {
            
        }
    }
}
