﻿namespace Bitz.Modules.Platform.Android.Graphics.Shaders.Stock
{
    public class Stock2D : Shader
    {
        public Stock2D()
        {

                VertexShader = @"
#version 300 es
uniform mat4 uPMatrix;
uniform mat4 uMVMatrix;

in vec4 aPosition;
in vec4 aDiffuseColour;
in vec2 aTexCoordinate;

out vec3 vPosition;
out vec4 vDiffuseColour;
out vec2 vTexCoordinate;

void main()
{
    vPosition = vec3(uMVMatrix*aPosition);
    vDiffuseColour = aDiffuseColour;
    vTexCoordinate = aTexCoordinate;
    gl_Position = uPMatrix * uMVMatrix * aPosition;
}
";

                FragementShader = @"
#version 300 es
precision mediump float;

uniform sampler2D primaryTexture;

in vec3 vPosition;        // Interpolated position for this fragment.
in vec4 vDiffuseColour;   // This is the diffuse color from the vertex shader interpolated across the triangle per fragment.
in vec2 vTexCoordinate;   // Interpolated texture coordinate per fragment.

out vec4 fragmentColor;

void main()
{
    fragmentColor = texture( primaryTexture,vTexCoordinate) * vDiffuseColour;
}
";
            
        }

        public override void PreDraw()
        {
        }
    }
}