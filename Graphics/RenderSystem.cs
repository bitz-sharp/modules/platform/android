using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Pack;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Graphics.Shaders;
using OpenTK;
using Boolean = System.Boolean;
using OpenTK.Graphics.ES20;
using System.Threading;
using Bitz.Modules.Core.Graphics.Canvases;
using Bitz.Modules.Platform.Android.Graphics.Canvas;
using Bitz.Modules.Platform.Android.Platform;

namespace Bitz.Modules.Platform.Android.Graphics
{
    public class RenderSystem : BasicObject, IRenderSystem
    {
        private readonly List<Tuple<Int32, SourceTexture>> _ActiveTextures = new List<Tuple<Int32, SourceTexture>>();

        private readonly Dictionary<SourceTexture, Int32?> _SourceTextureToGLID = new Dictionary<SourceTexture, Int32?>();
        private IWindowInstance _WindowInstance;

        private IShader _CurrentBoundShader;

        private Boolean _VBOEnabledPointerDiffuseColours;
        private Boolean _VBOEnabledPointerTextureCoords;
        private Boolean _VBOEnabledPointerVerts;
        private Boolean _VBOEnabledPointerNormals;
        private Boolean _VBOEnabledPointerMaterialIDs;

        private Int32 _VBOPointerDiffuseColours = -1;
        private Int32 _VBOPointerTextureCoords = -1;
        private Int32 _VBOPointerVerts = -1;
        private Int32 _VBOPointerNormals = -1;
        private Int32 _VBOPointerMaterialIDs = -1;

        private Boolean _VSyncActive = true;
        private Boolean _TestBool;
        private Boolean _ViewportUpdateRequired = true;

        private Vector3[] _VBODataVerts = new Vector3[4];
        private Vector4[] _VBODataDiffuseColours = new Vector4[4];
        private Vector2[] _VBODataTextureCoordinates = new Vector2[4];
        private Vector3[] _VBODataNormals = new Vector3[4];
        private Int32[] _VBODataMaterialIDs = new Int32[4];
        private Int32 _DebugCountDrawable;
        private Int32 _DebugCountRenterIntervals;
        private Int32 _DebugCountScenes;

        public RenderSystem()
        {

        }

        public Boolean VSyncActive
        {
            get => _VSyncActive;
            set
            {
                _VSyncActive = value;
                _WindowInstance.SetSwapInterval(_VSyncActive ? 1 : 0);
            }
        }

        public Int32 DebugCountDrawable
        {
            get => _DebugCountDrawable;
            private set => _DebugCountDrawable = value;
        }

        public Int32 DebugCountRenterIntervals
        {
            get => _DebugCountRenterIntervals;
            private set => _DebugCountRenterIntervals = value;
        }

        public Int32 DebugCountScenes
        {
            get => _DebugCountScenes;
            private set => _DebugCountScenes = value;
        }

        public void Initialize()
        {
            _WindowInstance = Injector.GetSingleton<IWindowInstance>();
            while (!_WindowInstance.IsWindowReady())
            {
                Thread.Sleep(TimeSpan.Zero);
            }
            Boolean activityLocked = false;
            BitzActivity.ActivityLock.Enter(ref activityLocked);

            (_WindowInstance as WindowInstance).MakeCurrent((_WindowInstance as WindowInstance).GetWindowInfo());
            VSyncActive = true;
            _WindowInstance.SetVisible(true);

            (_WindowInstance as WindowInstance).MakeCurrent(null);
            BitzActivity.ActivityLock.Exit();
        }

        internal void MarkViewportDirty()
        {
            _ViewportUpdateRequired = true;
        }

        public virtual void UpdateViewport()
        {

            Vector2 windowDimentions = _WindowInstance.GetWindowDimentions();
            GL.Viewport(0, 0, (Int32)windowDimentions.X, (Int32)windowDimentions.Y);
            _ViewportUpdateRequired = false;

        }

        public override void Dispose()
        {
            _WindowInstance.Dispose();
            _WindowInstance = null;
            base.Dispose();
        }

        public void ProcessWindowMessages()
        {
            _WindowInstance.ProcessWindowsEvents();
        }

        public IWindowInstance GetWindowInstance()
        {
            return _WindowInstance;
        }

        public void SetResolution(Vector2 newResolution)
        {
            _WindowInstance.Resolution = newResolution;
        }

        public void Draw(IEnumerable<Core.Graphics.Canvases.Canvas> canvasesToDraw)
        {
            _DebugCountDrawable = 0;
            _DebugCountRenterIntervals = 0;
            _DebugCountScenes = 0;

            Boolean activityLocked = false;
            BitzActivity.ActivityLock.Enter(ref activityLocked);
            (_WindowInstance as WindowInstance).MakeCurrent((_WindowInstance as WindowInstance).GetWindowInfo());

            _TestBool = !_TestBool;
            GL.ClearColor(Injector.GetSingleton<IGraphicsService>().DebugMode ? 0.5f : 0.0f, 0.0f, 0.0f, 1f);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (Core.Graphics.Canvases.Canvas canvas in canvasesToDraw.OrderBy(a => a.Order))
            {
                canvas.ConfigureGlState();
                _DebugCountScenes++;

                if (_CurrentBoundShader != canvas.DefaultShader)
                {
                    canvas.DefaultShader.Bind();
                    _CurrentBoundShader = canvas.DefaultShader;
                }
                if (_ViewportUpdateRequired) UpdateViewport();

                canvas.DefaultShader.SetVariable("uPMatrix", canvas.PrimaryCamera.ProjectionMatrix);
                canvas.DefaultShader.SetVariable("uMVMatrix", canvas.PrimaryCamera.ViewMatrix);

                canvas.DefaultShader.InternalPreDraw();

                if (_VBOPointerVerts == -1) _VBOPointerVerts = GL.GetAttribLocation(canvas.DefaultShader.ProgramID, "aPosition");
                if (_VBOPointerDiffuseColours == -1) _VBOPointerDiffuseColours = GL.GetAttribLocation(canvas.DefaultShader.ProgramID, "aDiffuseColour");
                if (_VBOPointerTextureCoords == -1) _VBOPointerTextureCoords = GL.GetAttribLocation(canvas.DefaultShader.ProgramID, "aTexCoordinate");
                if (_VBOPointerNormals == -1) _VBOPointerNormals = GL.GetAttribLocation(canvas.DefaultShader.ProgramID, "aNormal");
                if (_VBOPointerMaterialIDs == -1) _VBOPointerMaterialIDs = GL.GetAttribLocation(canvas.DefaultShader.ProgramID, "aMaterialID");

                List<RenderDataRef> renderIntervalList = PrepareDrawData(canvas);

                _DebugCountDrawable += renderIntervalList.Count;

                if (canvas is ICanvas2D) Draw2D(renderIntervalList);
                if (canvas is ICanvas3D) Draw3D(renderIntervalList, (Canvas3D)canvas);
            }

            DisableVBOPointerIfEnabled(ref _VBOEnabledPointerVerts, ref _VBOPointerVerts);
            DisableVBOPointerIfEnabled(ref _VBOEnabledPointerDiffuseColours, ref _VBOPointerDiffuseColours);
            DisableVBOPointerIfEnabled(ref _VBOEnabledPointerTextureCoords, ref _VBOPointerTextureCoords);
            DisableVBOPointerIfEnabled(ref _VBOEnabledPointerNormals, ref _VBOPointerNormals);
            DisableVBOPointerIfEnabled(ref _VBOEnabledPointerMaterialIDs, ref _VBOPointerMaterialIDs);

            try
            {
                _WindowInstance.SwapBuffers();
            }
            catch
            {
            }

            (_WindowInstance as WindowInstance).MakeCurrent(null);
            BitzActivity.ActivityLock.Exit();
        }

        private void Draw2D(IReadOnlyList<RenderDataRef> renderIntervalList)
        {
            for (Int32 index = 0; index < renderIntervalList.Count; index++)
            {
                List<UInt32> indexes = new List<UInt32>(renderIntervalList.Count * 4);
                SourceTexture sourceTexture = renderIntervalList[index].SourceTexture;
                indexes.AddRange(renderIntervalList[index].OffsetIndicies);
                while (renderIntervalList.Count > index + 1
                       && renderIntervalList[index].SourceTexture == renderIntervalList[index + 1].SourceTexture
                       && renderIntervalList[index].ClipperActive == renderIntervalList[index + 1].ClipperActive
                       && renderIntervalList[index].ClipperBounds == renderIntervalList[index + 1].ClipperBounds
                )
                {
                    indexes.AddRange(renderIntervalList[index + 1].OffsetIndicies);
                    index++;
                }

                SetActiveTexture(sourceTexture);
                UInt16[] indicies = indexes.Select(a=>(UInt16)a).ToArray();
                GL.DrawElements(PrimitiveType.Triangles, indicies.Length, DrawElementsType.UnsignedShort, indicies);
                _DebugCountRenterIntervals++;
            }
        }

        private void Draw3D(IEnumerable<RenderDataRef> renderIntervalList, Canvas3D currentCanvas)
        {
            foreach (RenderDataRef interval in renderIntervalList)
            {
                SourceTexture sourceTexture = interval.SourceTexture;
                SetActiveTexture(sourceTexture);

                Vector3[] specularColour = interval.Materials.Select(a => a.SpecularColour).ToArray();
                Single[] specularHardnessr = interval.Materials.Select(a => a.SpecularHardness).ToArray();

                currentCanvas.DefaultShader.SetVariable("MAT_SpecularColour", specularColour);
                currentCanvas.DefaultShader.SetVariable("MAT_SpecularHardness", specularHardnessr);
                currentCanvas.DefaultShader.InternalPreDraw();

                UInt16[] ind = interval.OffsetIndicies.Select(a => (UInt16) a).ToArray();

                GL.DrawElements(PrimitiveType.Triangles, interval.Indices.Length, DrawElementsType.UnsignedShort, ind);
                _DebugCountRenterIntervals++;
            }
        }

        private void DisableVBOPointerIfEnabled(ref Boolean enabledFlag, ref Int32 vboPointer)
        {
            if (!enabledFlag) return;
            GL.DisableVertexAttribArray(vboPointer);
            enabledFlag = false;
        }

        private void SetActiveTexture(SourceTexture sourceTexture, Int32 textureUnit = 0)
        {
            if (_ActiveTextures.Any(a => a.Item1 == textureUnit && a.Item2 == sourceTexture)) return;

            //Set active texture
            Int32? currentMapping = _SourceTextureToGLID.ContainsKey(sourceTexture) ? _SourceTextureToGLID[sourceTexture] : null;

            if (currentMapping != null)
            {
                GL.BindTexture(TextureTarget.Texture2D, currentMapping.Value);
            }
            else
            {
                Int32 texId = GL.GenTexture();
                GL.BindTexture(TextureTarget.Texture2D, texId);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (Single)All.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (Single)All.Linear);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (Single)All.ClampToEdge);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (Single)All.ClampToEdge);

                switch (sourceTexture.CompressionFormat)
                {
                    case TexturePack.CompressionFormat.RAW:
                        GL.TexImage2D(TextureTarget2d.Texture2D, 0, TextureComponentCount.Rgba, sourceTexture.Width, sourceTexture.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, ((byte[])sourceTexture.SourceTextureData));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _SourceTextureToGLID.Add(sourceTexture, texId);
            }

            _ActiveTextures.RemoveAll(a => a.Item1 == textureUnit);
            _ActiveTextures.Add(new Tuple<Int32, SourceTexture>(textureUnit, sourceTexture));
        }

        private List<RenderDataRef> PrepareDrawData(Core.Graphics.Canvases.Canvas canvas)
        {
            Int32 vertPos = 0;
            Int32 diffuseColPos = 0;
            Int32 tcPos = 0;
            Int32 normalPos = 0;
            Int32 materialIDPos = 0;

            List<RenderDataRef> list = canvas.RegisteredRenderDefs.AsParallel().Where(rdr =>
            {
                rdr.PreDrawHook?.Invoke();
                return rdr.Visible;
            })
            .ToList().OrderBy(a => a.ZOrder)
            .Select(def => def.UpdateRenderData(this, ref vertPos, ref _VBODataVerts, ref diffuseColPos, ref _VBODataDiffuseColours, ref tcPos, ref _VBODataTextureCoordinates, ref normalPos, ref _VBODataNormals, ref materialIDPos, ref _VBODataMaterialIDs))
            .Where(a => a?.SourceTexture != null)
            .ToList();

            if (_VBOPointerVerts != -1)
            {
                GL.VertexAttribPointer(_VBOPointerVerts, 3, VertexAttribPointerType.Float, false, 0, _VBODataVerts);
                if (!_VBOEnabledPointerVerts)
                {
                    GL.EnableVertexAttribArray(_VBOPointerVerts);
                    _VBOEnabledPointerVerts = true;
                }
            }
            else DisableVBOPointerIfEnabled(ref _VBOEnabledPointerVerts, ref _VBOPointerVerts);

            if (_VBOPointerNormals != -1)
            {
                GL.VertexAttribPointer(_VBOPointerNormals, 3, VertexAttribPointerType.Float, false, 0, _VBODataNormals);
                if (!_VBOEnabledPointerNormals)
                {
                    GL.EnableVertexAttribArray(_VBOPointerNormals);
                    _VBOEnabledPointerNormals = true;
                }
            }
            else DisableVBOPointerIfEnabled(ref _VBOEnabledPointerNormals, ref _VBOPointerNormals);

            if (_VBOPointerDiffuseColours != -1)
            {
                GL.VertexAttribPointer(_VBOPointerDiffuseColours, 4, VertexAttribPointerType.Float, false, 0, _VBODataDiffuseColours);
                if (!_VBOEnabledPointerDiffuseColours)
                {
                    GL.EnableVertexAttribArray(_VBOPointerDiffuseColours);
                    _VBOEnabledPointerDiffuseColours = true;
                }
            }
            else DisableVBOPointerIfEnabled(ref _VBOEnabledPointerDiffuseColours, ref _VBOPointerDiffuseColours);

            if (_VBOPointerTextureCoords != -1)
            {
                GL.VertexAttribPointer(_VBOPointerTextureCoords, 2, VertexAttribPointerType.Float, false, 0, _VBODataTextureCoordinates);
                if (!_VBOEnabledPointerTextureCoords)
                {
                    GL.EnableVertexAttribArray(_VBOPointerTextureCoords);
                    _VBOEnabledPointerTextureCoords = true;
                }
            }
            else DisableVBOPointerIfEnabled(ref _VBOEnabledPointerTextureCoords, ref _VBOPointerTextureCoords);

            if (_VBOPointerMaterialIDs != -1)
            {
                GL.VertexAttribPointer(_VBOPointerMaterialIDs, 1, VertexAttribPointerType.Float, false, 0, _VBODataMaterialIDs);
                if (!_VBOEnabledPointerMaterialIDs)
                {
                    GL.EnableVertexAttribArray(_VBOPointerMaterialIDs);
                    _VBOEnabledPointerMaterialIDs = true;
                }
            }
            else DisableVBOPointerIfEnabled(ref _VBOEnabledPointerMaterialIDs, ref _VBOPointerMaterialIDs);

            return list;
        }

        internal void MakeCurrent()
        {
            WindowInstance wi = (_WindowInstance as WindowInstance);
            wi.MakeCurrent(wi.GetWindowInfo());
        }

        internal void MakeNotCurrent()
        {
            WindowInstance wi = (_WindowInstance as WindowInstance);
            wi.MakeCurrent(null);
        }

        internal enum ArrayTypes
        {
            VERT,
            COLOUR,
            TC
        }
    }
}