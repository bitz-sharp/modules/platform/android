﻿using System;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Platform.Android.Platform;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Platform;

namespace Bitz.Modules.Platform.Android.Graphics
{
    public class WindowInstance : IWindowInstance
    {
        private static IGraphicsContext _GraphicsContext;
        internal static Boolean RenderingSafe = false;
        protected Vector2 _CurrentResolution;
        protected Vector2 _TargetResolution;
        protected Boolean _IsVisible;
        protected Boolean _FullScreen;
        protected Boolean _TargetFullScreen;


        public WindowInstance()
        {
            _IsVisible = false;
        }

        internal void OnScreenChange()
        {
            var gfxInstance = Injector.GetSingleton<IGraphicsService>();
            gfxInstance.InvalidateAllCameraMatrix();
            _CurrentResolution = GetWindowDimentions();
            (gfxInstance.RenderSystem as RenderSystem).MarkViewportDirty();
        }

        public Vector2 Resolution
        {
            get => _CurrentResolution;
            set => throw new NotSupportedException();
        }

        /// <inheritdoc />
        public Boolean Focused => true;

        internal INativeWindow GetNativeWindow()
        {
            return BitzActivity.View;
        }

        public void SetVisible(Boolean visible)
        {
            _IsVisible = visible;
        }

        public IWindowInfo GetWindowInfo()
        {
            return BitzActivity.View.WindowInfo;
        }

        public void ProcessWindowsEvents()
        {

        }

        public void SetFullScreen(Boolean active)
        {
        }

        public Vector2 GetWindowDimentions()
        {
            return new Vector2(BitzActivity.View.Width, BitzActivity.View.Height); ;
        }

        public void SetSwapInterval(Int32 newInterval)
        {
          //  if (_GraphicsContext != null) _GraphicsContext.SwapInterval = 1;
        }

        public void SwapBuffers()
        {
            if (!RenderingSafe) return;
            try
            {
                _GraphicsContext?.SwapBuffers();
            }
            catch
            {
                // ignored
            }
        }

        public void MakeCurrent(IWindowInfo window)
        {
            _GraphicsContext?.MakeCurrent(window);
        }

        public Boolean IsWindowReady()
        {
            return _GraphicsContext != null && RenderingSafe;
        }

        internal static void SetContext(IGraphicsContext context)
        {
            _GraphicsContext = context;
        }

        public void Initialize()
        {
        }

        public void Dispose()
        {
        }
    }
}