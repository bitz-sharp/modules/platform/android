using System;
using System.Linq;
using Android.Util;
using Bitz.Modules.Core.Debug.Remoting;
using Sbatman.Serialize;

namespace Bitz.Modules.Platform.Android.Debug
{
    class RemoteInterface : IRemoteInterface
    {
        public void Update()
        {

        }

        public void SendMessage(RemoteMessage message)
        {
            Packet serviceData = Packet.FromByteArray(Convert.FromBase64String(message.Message));
            foreach (String s in serviceData.GetObjects().Cast<String>())
            {
                Log.WriteLine(LogPriority.Verbose, "BITZ-REMOTE", (s.Replace(',', '\t')));
            }
        }

        public void Connect(String host, Int32 port)
        {
        }

        public void Disconnect()
        {
        }

        public void Initialize()
        {
            
        }

        public Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            return false;
        }

        public void Dispose()
        {
        }
    }
}