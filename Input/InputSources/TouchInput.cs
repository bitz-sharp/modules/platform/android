using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Android.Views;
using OpenTK;
using System;
using Bitz.Modules.Core.Input;
using InputEvent = Bitz.Modules.Core.Input.InputEvent;

namespace Bitz.Modules.Platform.Android.Input.InputSources
{
    internal class TouchInput : IInputSource
    {
        internal static TouchInput TouchInputInstance;
        private static readonly ReaderWriterLockSlim _Lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

        private readonly List<InputEvent> _Touches = new List<InputEvent>();

        public TouchInput()
        {
            Lock();
            TouchInputInstance = this;
            Unlock();
        }

        public void ProcessInputEvents(List<InputEvent> activeTouchEvents)
        {
            Lock();
            activeTouchEvents.Clear();
            activeTouchEvents.AddRange(_Touches);
            Unlock();
        }

        internal void OnTouchEvent(MotionEvent e)
        {
            Int32 pointerIndex = (Int32)(e.Action & MotionEventActions.PointerIdMask) >> (Int32)MotionEventActions.PointerIdShift;
            Int32 id = e.GetPointerId(pointerIndex);
            InputEvent assignedInputEvent = _Touches.FirstOrDefault(a => a.ID == id);

            MotionEventActions action = e.Action & MotionEventActions.Mask;

            if (assignedInputEvent == null && (action == MotionEventActions.Down || action == MotionEventActions.PointerDown) && e.PointerCount > 0)
            {
                assignedInputEvent = new InputEvent()
                {
                    Active = action == MotionEventActions.Down,
                    ID = id,
                    Position = new Vector2(e.GetX(id), e.GetY(id)),
                    StartPosition = new Vector2(e.GetX(id), e.GetY(id)),
                    TypeOfInput = InputEvent.InputType.LEFT_CLICK
                };
                _Touches.Add(assignedInputEvent);
            }
            else if (assignedInputEvent != null && (action == MotionEventActions.Up || action == MotionEventActions.PointerUp || action == MotionEventActions.Cancel || action == MotionEventActions.Outside))
            {
                if( e.PointerCount > 0)assignedInputEvent.Position = new Vector2(e.GetX(id), e.GetY(id));
                assignedInputEvent.Complete();
                _Touches.Remove(assignedInputEvent);
            }
            else if (assignedInputEvent != null && action == MotionEventActions.Move)
            {
                assignedInputEvent.Position = new Vector2(e.GetX(id), e.GetY(id));
            }
        }

        public void Dispose()
        {
            Lock();
            TouchInputInstance = null;
            Unlock();
        }

        public static void Lock()
        {
            _Lock.EnterWriteLock();
        }

        public static void Unlock()
        {
            _Lock.ExitWriteLock();
        }

        public void Initialize()
        {

        }
    }
}