using System;
using System.Collections.Generic;
using Android.Util;
using Bitz.Modules.Core.Foundation.Debug.Logging;

namespace Bitz.Engine.Android.Utility.Logging.Consumers
{
    class LogConsumerAndroid : ILogConsumer
    {
        public void Dispose()
        {

        }

        public Boolean HasInit()
        {
            return true;
        }

        public void Init()
        {
        }

        public void ConsumeLogEvent(LogEvent logEvent)
        {
            LogPriority priority;

            switch (logEvent.Severity)
            {
                case LogSeverity.VERBOSE: priority = LogPriority.Verbose; break;
                case LogSeverity.DEBUG: priority = LogPriority.Debug; break;
                case LogSeverity.WARNING: priority = LogPriority.Warn; break;
                case LogSeverity.ERROR: priority = LogPriority.Error; break;
                case LogSeverity.CRITICAL: priority = LogPriority.Error; break;
                case LogSeverity.CUSTOM1: priority = LogPriority.Verbose; break;
                case LogSeverity.CUSTOM2: priority = LogPriority.Verbose; break;
                case LogSeverity.CUSTOM3: priority = LogPriority.Verbose; break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Log.WriteLine(priority, "Bitz", $"{logEvent.Message}");
        }

        public void ConsumeLogEvent(List<LogEvent> logEvents)
        {
            foreach (LogEvent lEvent in logEvents) ConsumeLogEvent(lEvent);
        }

        public void Initialize()
        {
            
        }
    }
}