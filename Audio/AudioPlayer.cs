﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bitz.Modules.Core.Audio;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK.Audio.OpenAL;

namespace Bitz.Modules.Platform.Android.Audio
{
    public class AudioPlayer : BasicObject, IAudioPlayer
    {
        public Channels Channel { get; set; }

        internal Channels AssignedChannel { get; set; }

        internal IAudioData Data { get; set; }

        internal Int32 SourceId { get; set; }

        public Boolean Playing { get; internal set; }

        private Boolean _Looping;
        public Boolean Looping
        {
            get => _Looping;
            set => _Looping = value;
        }

        private Single _Pitch = 1f;
        public Single Pitch
        {
            get => _Pitch;
            set => _Pitch = value;
        }

        public Boolean Paused { get; internal set; }

        private Single _Volume = 1f;

        public Single Volume
        {
            get => _Volume;
            set => _Volume = value;
        }

        public Boolean DisposeOnFinish { get; internal set; }

        private readonly List<Int32> _LoadedBufferIds = new List<Int32>();

        public AudioPlayer(String path, Boolean playOnStart = false, Channels channel = Channels.CHANNEL_ANY, Boolean looping = false, Boolean disposeOnFinish = true)
            : this((Injector.GetSingleton<IAudioService>() as AudioService).GetData( path), playOnStart, channel, looping, disposeOnFinish) { }

        public AudioPlayer(IAudioData data, Boolean playOnStart = false, Channels channel = Channels.CHANNEL_ANY, Boolean looping = false, Boolean disposeOnFinish = true)
        {
            Data = data;
            Channel = channel;
            Looping = looping;
            DisposeOnFinish = disposeOnFinish;

            if (playOnStart) Play();
        }

        public void Update()
        {
            if (AL.GetSourceState(SourceId) == ALSourceState.Stopped)
            {
                Stop();
                return;
            }

            UpdateProperties();


        }

        public void Play()
        {
            if (Playing) return;

            Playing = true;

            (Injector.GetSingleton<IAudioService>() as AudioService).RegisterAudioPlayer(this);
            UpdateProperties();

            if (_LoadedBufferIds.Count == 0)
            {
                AL.SourceQueueBuffer(SourceId, Data.BufferIds[0]);
                _LoadedBufferIds.Add(Data.BufferIds[0]);
            }
            if (Data.BufferIds.Length > 1) EnsureBufferFilled();

            AL.SourcePlay(SourceId);
        }

        public void Stop(Boolean unRegister = false)
        {
            if (SourceId == -1) return;
            Playing = false;

            AL.SourceStop(SourceId);

            SourceId = -1;

            AL.SourceUnqueueBuffers(SourceId, _LoadedBufferIds.Count, _LoadedBufferIds.ToArray());

            _LoadedBufferIds.Clear();

            if (unRegister) return;

            (Injector.GetSingleton<IAudioService>() as AudioService).UnregisterAudioPlayer(this);

            if (DisposeOnFinish)
            {
            }
        }

        public void Pause()
        {
            AL.SourcePause(SourceId);
        }

        public void Resume()
        {
            AL.SourcePlay(SourceId);
        }

        private void UpdateProperties()
        {
            AL.Source(SourceId, ALSourceb.Looping, _Looping);
            AL.Source(SourceId, ALSourcef.Pitch, _Pitch);
            AL.Source(SourceId, ALSourcef.Gain, _Volume);
        }

        private void EnsureBufferFilled()
        {
            if (SourceId == -1) return;

            foreach (Int32 dataBufferId in Data.BufferIds)
            {
                if (!_LoadedBufferIds.Contains(dataBufferId) && (Data as AudioData).LoadedIds.Contains(dataBufferId))
                {
                    Pause();
                    AL.SourceQueueBuffer(SourceId, dataBufferId);
                    Resume();
                    _LoadedBufferIds.Add(dataBufferId);
                }
            }

            if (_LoadedBufferIds.Count != Data.BufferIds.Length) Task.Run(EnsureBufferFilled);
        }
    }
}
