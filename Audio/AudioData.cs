﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bitz.Modules.Core.Assets;
using Bitz.Modules.Core.Audio;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Platform.Android.Platform;
using NVorbis;
using OpenTK.Audio.OpenAL;

namespace Bitz.Modules.Platform.Android.Audio
{
    public class AudioData : IAudioData
    {
        /// <summary> Store for the sound data </summary>
        public Byte[] SoundData { get; internal set; }
        /// <summary> ALFormat for the sound </summary>
        public ALFormat Format { get; internal set; }
        /// <summary> The amount of channels used for the sound </summary>
        public Int32 AudioChannels { get; internal set; }
        /// <summary> Audio bit rate </summary>
        public Int32 BitRate { get; internal set; }
        /// <summary> Sample rate for the sound </summary>
        public Int32 SampleRate { get; internal set; }
        /// <summary> Ids for the buffers </summary>
        public Int32[] BufferIds { get; }
        /// <summary> List of loaded ids </summary>
        internal readonly List<Int32> LoadedIds = new List<Int32>();
        /// <summary> Boolean to see if the sound is completely loaded </summary>
        internal Boolean LoadComplete => LoadedIds.Count == BufferIds.Length;
        /// <summary> Store for the file path </summary>
        public String FilePath { get; private set; }

        /// <summary> Constructor for the audio data </summary>
        /// <param name="path">File path for the sound</param>
        internal AudioData(String path)
        {
            path = Regex.Replace(path, @"^Content\\\s*", String.Empty, RegexOptions.Multiline);
            string LoadPath = path.Replace('\\', '/');

            Byte[] dataArray = Injector.GetSingleton<IFileHandler>().GetFile(LoadPath);

            MemoryStream memStream = new MemoryStream(dataArray);

            VorbisReader reader = new VorbisReader(memStream, true);

            Single buffersRequired = (Single)(reader.TotalTime.TotalSeconds / TimeSpan.FromSeconds(5).TotalSeconds);

            BufferIds = AL.GenBuffers((Int32)Math.Ceiling(buffersRequired));
            SoundData = new Byte[reader.TotalSamples * reader.Channels * 2];

            AudioChannels = reader.Channels;
            BitRate = 16;
            SampleRate = reader.SampleRate;
            Format = AudioChannels == 1 ? ALFormat.Mono16 : ALFormat.Stereo16;

            FilePath = path;

            BufferData(reader, 0, BufferIds.ToList(), buffersRequired, true);

            memStream.Dispose();
        }

        /// <summary> Buffers the data for the sound </summary>
        /// <param name="reader">Vorbis reader</param>
        /// <param name="pos">Position into buffering the sound</param>
        /// <param name="bufferIds">List of the all buffer Ids</param>
        /// <param name="bufferscale">Single scale for the number of buffers that are required</param>
        /// <param name="first">boolean to flag to state if this is the first run of this function</param>
        private void BufferData(VorbisReader reader, Int32 pos, List<Int32> bufferIds, Single bufferscale, Boolean first = false)
        {
            Action action = () =>
            {
                if (bufferIds.Count == 0) { reader.Dispose(); return; }
                System.Diagnostics.Debug.Assert(SoundData != null, "SoundData != null");
                Int32 bufferSize = (Int32)(SoundData.Length / 2.0 / bufferscale);

                if (bufferIds.Count == 1) bufferSize = (SoundData.Length - pos) / 2;

                Single[] buffer = new Single[bufferSize];

                reader.ReadSamples(buffer, 0, buffer.Length);
                Byte[] data = ConvertData(buffer);

                for (Int32 i = 0; i < data.Length; i++) { SoundData[pos + i] = data[i]; }

                pos += data.Length;

                Int32 bufferId = bufferIds[0];
                bufferIds.RemoveAt(0);

                AL.BufferData(bufferId, Format, data, data.Length, SampleRate);
                LoadedIds.Add(bufferId);
                BufferData(reader, pos, bufferIds, bufferscale);
            };

            if (first) action.Invoke();
            else Task.Run(action);
        }

        /// <summary> Converts the single sound data to byte data </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private Byte[] ConvertData(Single[] data)
        {
            Byte[] rtnArray = new Byte[data.Length * 2];

            for (Int32 i = 0; i < data.Length; i++)
            {
                Int16 p = (Int16)(data[i] * Int16.MaxValue);

                rtnArray[i * 2] = (Byte)(p & 255);
                rtnArray[i * 2 + 1] = (Byte)((p >> 8) & 255);
            }

            return rtnArray;
        }
    }
}
