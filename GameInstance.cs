﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Bitz.Modules.Core.Logic.StateSystem;
using Bitz.Modules.Core.Assets;
using Bitz.Modules.Core.Debug;
using Bitz.Modules.Core.Debug.Logging;
using Bitz.Modules.Core.Debug.Remoting;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Graphics;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Foundation.Services;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Core.Graphics.Canvases;
using Bitz.Modules.Core.Input;
using Bitz.Modules.Core.Logic;
using Bitz.Modules.Platform.Android.Debug;
using Bitz.Modules.Platform.Android.Graphics;
using Bitz.Modules.Platform.Android.Graphics.Canvas;
using Bitz.Modules.Platform.Android.Input.InputSources;
using Bitz.Modules.Android.Platform.Assets;
using Bitz.Modules.Core.Audio;
using Bitz.Modules.Platform.Android.Audio;

namespace Bitz.Modules.Platform.Android
{
    public abstract class GameInstance : BasicObject
    {
        private readonly Stopwatch _EngineTime;

        private Boolean _Running;

        private readonly ILoggerService _LoggerService;
        private readonly IGameLogicService _GameLogicService;
        private readonly IGraphicsService _GraphicsService;

        protected GameInstance(ScreenLayout desiredLayout, LogSeverity loggerSeverity = LogSeverity.WARNING | LogSeverity.ERROR | LogSeverity.CRITICAL, String gameName = "Bitz", Boolean logToDebug = false)
        {

            Injector.RegisterMapping<IGraphicsService>(typeof(GraphicsService), true);
            Injector.RegisterMapping<IWindowInstance>(typeof(WindowInstance), true);
            Injector.RegisterMapping<IRenderSystem>(typeof(RenderSystem), true);
            Injector.RegisterMapping<ICanvas2D>(typeof(Canvas2D), true);
            Injector.RegisterMapping<ICanvas3D>(typeof(Canvas3D), true);

            Injector.RegisterMapping<IAudioService>(typeof(AudioService), true);


            Injector.RegisterMapping<IInputService>(typeof(InputService), true);
            Injector.RegisterMultiMapping<IInputSource>(typeof(TouchInput), true);


            Injector.RegisterMapping<IGameLogicService>(typeof(GameLogicService), true);
            Injector.RegisterMapping<IStateService>(typeof(StateService), true);
            Injector.RegisterMapping<ILoggerService>(typeof(LoggerService), true);

            Injector.RegisterMapping<IFileHandler>(typeof(FileHandler), true);
            Injector.RegisterMapping<IAssetsService>(typeof(AssetsService), true);

            Injector.RegisterMapping<IRemoteInterface>(typeof(RemoteInterface), true);


            if (Instance != null)
            {

                throw new InvalidOperationException($"You cannot create an instance of {nameof(GameInstance)} while another exists");
            }

            Instance = this;
            GameName = gameName;

            _GraphicsService = Injector.GetSingleton<IGraphicsService>();
            _LoggerService = Injector.GetSingleton<ILoggerService>();
            _LoggerService.ActiveSeverityLevels = loggerSeverity;
            _GameLogicService = Injector.GetSingleton<IGameLogicService>();

            _GraphicsService.SetRequestedLayout(desiredLayout);

            _EngineTime = Stopwatch.StartNew();

        }

        // public GraphicsService.ScreenLayout RequestedScreenLayout { get; } TODO - Get from graphics service

        public String GameName { get; }

        /// <summary>
        ///     A boolean representing whether the game instance is running or not
        /// </summary>
        public Boolean Running => _Running;

        /// <summary>
        ///     Returns a timespan representing the time the GameInstance has been running for
        /// </summary>
        public TimeSpan EngineTime
        {
            get
            {
                if (IsDisposed()) throw new ObjectDisposedException(nameof(GameInstance));
                return _EngineTime.Elapsed;
            }
        }

        /// <summary>
        ///     The current individual game instance running in this app domain
        /// </summary>
        public static GameInstance Instance { get; private set; }

        public override void Dispose()
        {

            if (Instance == this) Instance = null;
            base.Dispose();
        }

        protected abstract void OnInit();
        protected abstract void OnExit();


        public virtual void Exit()
        {
            _Running = false;
        }

        public void Run()
        {
            if (Injector.GetSingleton<IDebugService>() == null)
            {
                System.Diagnostics.Debug.Print("Debug Service undefined\n");
            }
            System.Diagnostics.Debug.Print("Game Instance Run Begin\n");
            if (IsDisposed()) throw new ObjectDisposedException(nameof(GameInstance));


            LogEngineVersion();

            _LoggerService.Log(LogSeverity.VERBOSE, "Performing GameInstance OnInit");

            OnInit();

            _LoggerService.Log(LogSeverity.VERBOSE, "Beginning Main Loop");

            System.Diagnostics.Debug.Print("Game Instance Entering while loop\n");



            _Running = true;
            while (_Running)
            {
                if (_Running && !IsDisposed())
                {
                    _GameLogicService.TriggerUpdate(_EngineTime.Elapsed);
                    _GraphicsService.Draw();
                }
                Thread.Sleep(0);
            }

            System.Diagnostics.Debug.Print("Game Instance Exiting while loop\n");

            _LoggerService.Log(LogSeverity.VERBOSE, "Performing GameInstance OnExit");

            OnExit();

            ServiceManager.ShutdownServices();

            System.Diagnostics.Debug.Print("Game Instance Disposing for Exit\n");
            Dispose();
        }

        private static void LogEngineVersion()
        {
            AssemblyName assemblyInfo = new AssemblyName(typeof(GameInstance).GetTypeInfo().Assembly.FullName);
            Injector.GetSingleton<ILoggerService>().Log(LogSeverity.WARNING, $"Bitz Engine V{assemblyInfo.Version.Major}.{assemblyInfo.Version.Minor}.{assemblyInfo.Version.Build}.{assemblyInfo.Version.Revision}");
        }
    }
}