using System;
using Android.Content;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Platform.Android.Graphics;
using OpenTK.Graphics;
using OpenTK.Platform.Android;

namespace Bitz.Modules.Platform.Android
{
    class GLView1 : AndroidGameView
    {
        internal static Context _Context;
        public GLView1(Context context) : base(context)
        {
            _Context = context;
            ContextRenderingApi = GLVersion.ES3;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GraphicsContext.MakeCurrent(null);

            WindowInstance.SetContext(GraphicsContext);
        }

        protected override void CreateFrameBuffer()
        {
            try
            {
                ContextRenderingApi = GLVersion.ES3;
                base.CreateFrameBuffer();
                WindowInstance.SetContext(GraphicsContext);
                return;
            }
            catch (Exception ex)
            {
                Int32 g = 7;
            }
        }

        protected override void OnContextLost(EventArgs e)
        {
            WindowInstance.RenderingSafe = false;
            base.OnContextLost(e);
        }

        protected override void OnUnload(EventArgs e)
        {
            WindowInstance.RenderingSafe = false;
            base.OnUnload(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            (Injector.GetSingleton<IGraphicsService>().GetWindowInstance() as WindowInstance).OnScreenChange();
        }
    }
}