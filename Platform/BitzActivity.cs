using System;
using System.Threading;
using Android.App;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Graphics;
using Bitz.Modules.Platform.Android.Input.InputSources;
using WindowInstance = Bitz.Modules.Platform.Android.Graphics.WindowInstance;

namespace Bitz.Modules.Platform.Android.Platform
{

    public class BitzActivity : Activity
    {
        internal static GLView1 View;
        internal static BitzActivity CurrentActivity;
        internal static GameInstance GameInstance;
        private static Thread _GameThread;
        internal static SpinLock ActivityLock = new SpinLock();

        public BitzActivity(Type gameInstanceType)
        {
            if (GameInstance == null)
            {
                CurrentActivity = this;
                GameInstance = (GameInstance)Activator.CreateInstance(gameInstanceType);
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            View = new GLView1(this);
            CurrentActivity.SetContentView(View);

            Boolean lockTaken = false;
            ActivityLock.Enter(ref lockTaken);
            if (_GameThread == null)
            {
                _GameThread = new Thread(GameThread);
                _GameThread.Start();
            }
            ActivityLock.Exit();
            WindowInstance.RenderingSafe = true;
        }

        protected override void OnDestroy()
        {
            Boolean lockTaken = false;
            ActivityLock.Enter(ref lockTaken);
            WindowInstance.RenderingSafe = false;
            _GameThread = null;
            GameInstance.Exit();
            GameInstance = null;
            ActivityLock.Exit();

            base.OnDestroy();
        }

        public override void OnConfigurationChanged(Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            (Injector.GetSingleton<IGraphicsService>()?.GetWindowInstance() as WindowInstance)?.OnScreenChange();
        }

        protected override void OnPause()
        {
            Boolean lockTaken = false;
            ActivityLock.Enter(ref lockTaken);
            WindowInstance.RenderingSafe = false;
            // never forget to do this!
            base.OnPause();
            View?.Pause();
            WindowInstance.SetContext(null);
            ActivityLock.Exit();
        }

        private void GameThread()
        {
            GameInstance.Run();
        }

        public override Boolean OnTouchEvent(MotionEvent e)
        {
            TouchInput.Lock();

            if (TouchInput.TouchInputInstance == null)
            {
                TouchInput.Unlock();
                return base.OnTouchEvent(e);
            }

            TouchInput.TouchInputInstance.OnTouchEvent(e);

            TouchInput.Unlock();
            return base.OnTouchEvent(e);
        }

        protected override void OnResume()
        {
            Boolean lockTaken = false;
            ActivityLock.Enter(ref lockTaken);
            // never forget to do this!
            base.OnResume();
            View?.Resume();
            WindowInstance.RenderingSafe = true;
       
            ActivityLock.Exit();
        }
    }
}