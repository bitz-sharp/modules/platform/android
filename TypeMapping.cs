﻿using Bitz.Engine.Android.Debug;
using Bitz.Engine.Android.Graphics;
using Bitz.Engine.Android.Input.InputSources;
using Bitz.Engine.Android.Utility.Logging.Consumers;

namespace Bitz.Engine.Android
{
    static class TypeMapping
    {
        static TypeMapping()
        {
            //Shared.TypeMapping.Platform.System = typeof(Platform.System);
            Shared.TypeMapping.Debug.RemoteInterface = typeof(RemoteInterface);
            Shared.TypeMapping.Input.InputSources.Add(typeof(TouchInput));
            Shared.TypeMapping.Graphics.WindowInstance = typeof (WindowInstance);
            Shared.TypeMapping.Utility.DefaultLogConsumer = typeof(LogConsumerAndroid);
        }
    }
}
