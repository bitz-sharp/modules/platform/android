﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Bitz.Modules.Core.Assets;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Platform.Android.Platform;

namespace Bitz.Modules.Android.Platform.Assets
{
    class FileHandler : IFileHandler
    {
        private ILoggerService _LoggerService;
        /// <inheritdoc />
        public Byte[] GetFile(String filename)
        {
            filename = filename.Replace('\\', '/');
            filename = Regex.Replace(filename, @"^Content/\s*", String.Empty, RegexOptions.Multiline);

            String[] files = BitzActivity.CurrentActivity.Assets.List("Content");
            Stream data = BitzActivity.CurrentActivity.Assets.Open(filename);
            if (data == null)
            {
                _LoggerService.Log(LogSeverity.WARNING, $"Loading Graphics Pack from file '{filename}' failed as it was not detected on disk");
                return null;
            }
            return StreamToByteArray(data);
        }

        public Stream GetFileStream(String filename)
        {

            filename = filename.Replace('\\', '/');
            filename = Regex.Replace(filename, @"^Content/\s*", String.Empty, RegexOptions.Multiline);

            String[] files = BitzActivity.CurrentActivity.Assets.List("Graphics");
            Stream data = BitzActivity.CurrentActivity.Assets.Open(filename);
            if (data == null)
            {
                _LoggerService.Log(LogSeverity.WARNING, $"Loading Graphics Pack from file '{filename}' failed as it was not detected on disk");
                return null;
            }
            return data;

        }

        internal static Byte[] StreamToByteArray(Stream input)
        {
            Byte[] buffer = new Byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                Int32 read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        /// <inheritdoc />
        public void Initialize()
        {
            _LoggerService = Injector.GetSingleton<ILoggerService>();
        }
    }
}
